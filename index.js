const Queue = require('bull');
const nodemailer = require('nodemailer');
const HOST = process.env.HOST || '127.0.0.1';
const PORT = process.env.PORT|| 8080;
// 1. Initiating the Queue
console.log('1. Initiating the Queue');
const sendMailQueue = new Queue('sendMail', {
  redis: {
    host: HOST,
    port: PORT,
    password: 'root'
  }
});
const data = {
  email: 'umakant.suman@verizon.com'//'asha.james@verizon.com,koushik.x.dutta@verizon.com,sweta.x.ramprakash@verizon.com'
};

const options = {
  delay: 60000, // 1 min in ms
  attempts: 2
};
// 2. Adding a Job to the Queue
sendMailQueue.add(data, options);
console.log('2. Adding a Job to the Queue');

// 3. Consumer
sendMailQueue.process(async job => { 
  console.log('3 .Consumer');
    return await sendMail(job.data.email); 
  });
  function sendMail(email) {
    console.log('Enter in sendMail Method');
    return new Promise((resolve, reject) => {
      console.log('enter in promise');
      let mailOptions = {
        from: 'umakantsuman@gmail.com',
        to: email,
        subject: 'Bull - npm',
        text: "This email is from bull job scheduler Verizon Demo.",
      };
      let mailConfig = {
        service: 'gmail',
        auth: {
          user: 'umakantsuman@gmail.com',
          pass: 'userPass'
        }
      };
      nodemailer.createTransport(mailConfig).sendMail(mailOptions, (err, info) => {
        if (err) {
          console.log("Error!");
          reject(err);
        } else {
          resolve(info);//.catch( (ex)=>{console.log(`Exceptione ${ex}`)}
        }
      });
    });
  }